import "dart:io";
import "package:build/build.dart";
import "package:yaml/yaml.dart";

class VersionBuilder implements Builder {
  @override
  Future build(BuildStep step) async {
    final pubspecFile = loadYaml(
      await File('pubspec.yaml').readAsString(),
    );

    String fileContent = '''
//// AUTO-GENERATED by build_runner ////
/// DO NOT EDIT BY HAND
part of "version.dart";

const pubspecVersionString = '${pubspecFile["version"]}';
''';

    await step.writeAsString(step.allowedOutputs.first, fileContent);
  }

  @override
  final buildExtensions = const {
    "pubspec.yaml": [ "lib/shared/version.moxxy.dart" ]
  };
}

Builder versionBuilder(BuilderOptions _) => VersionBuilder();
